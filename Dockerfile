FROM alpine

COPY ./config /container_conf

VOLUME /config

ADD entrypoint.sh /usr/local/bin/entrypoint

RUN chmod +x /usr/local/bin/entrypoint

ENTRYPOINT [ "entrypoint" ]
